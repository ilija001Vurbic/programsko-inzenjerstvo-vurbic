def uppercase_count(string):
    """Return the number of uppercase letters inside the string.

    Arguments:
    string - input string which will be checked
    """
    count = 0
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    for char in string:
        if char in allowed_chars:
            count += 1
    return count

def main():
    strings = [
            "pero", 
            "Pero",
            "PEROPERO",
            "WySiWyG"
    ]   

    for string in strings:
        print(f"String '{string}' has {uppercase_count(string)} uppercase letters")

if __name__ == "__main__":
    main()

str = str (input ("Enter comma separated integers: "))
print (f"Input string: {str}")

list = str.split (",")
print (f"list: {list}")

li = []
for i in list:
	li.append(int(i))

print (f"list (li) : {li}")

def count_evens(list1):
    cnt=0
    for i in list1:
        if i%2==0:
            cnt+=1
    return cnt

def centered_average(list1):
    suma=0
    max_value=list1[0]
    min_value=list1[0]
    for i in list1:
        max_value=max(max_value,i)
        min_value=min(min_value,i)
        suma+=i
    return (suma-max_value-min_value)/(len(list1)-2)

def has_22(list2):
    for i in list2:
        if list2[i] and list2[i+1]==2:
            return True
        else:
            return False

def char_types_count(someString):
    up=sum(1 for ch in someString if ch.isupper())
    lw=sum(1 for ch in someString if ch.islower())
    nu=sum(1 for ch in someString if ch.isnumeric())
    return(up,lw,nu)
print(f"Even numbers in list: {count_evens(li)}")
print(f"Centered average in list: {centered_average(li)}")
print(f"List has 2 2's: {has_22(li)}")
string1 = input ("Enter string: ")
print(f"String has (uppercase, lowercase, numeric): {char_types_count(string1)}")
